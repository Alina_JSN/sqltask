exports.up = function(knex, Promise) {
    return knex.schema.createTable('user_group', function(table) {
        table.integer('user_id')
            .unsigned()
            .references('id')
            .inTable('users')
            .onDelete('CASCADE')
            .onUpdate('CASCADE')
        table.integer('group_id')
            .unsigned()
            .references('id')
            .inTable('group')
            .onDelete('CASCADE')
            .onUpdate('CASCADE')
    })
}

exports.down = function(knex, Promise) {
    return knex.schema.dropTable('user_group')
}