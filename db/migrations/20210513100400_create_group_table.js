exports.up = function(knex, Promise) {
    return knex.schema.createTable('group', function(table) {
        table.increments()
        table.string('group_name').notNullable()
    })
}

exports.down = function(knex, Promise) {
    return knex.schema.dropTable('group')
}