exports.up = function(knex, Promise) {
    return knex.schema.createTable('partners', function(table) {
        table.increments()
        table.string('partner_name').notNullable()
    })
}

exports.down = function(knex, Promise) {
    return knex.schema.dropTable('partners')
}