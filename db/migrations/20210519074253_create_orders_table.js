exports.up = function(knex, Promise) {
    return knex.schema.createTable('orders', function(table) {
        table.increments()
        table.integer('user_id')
            .unsigned()
            .references('id')
            .inTable('users')
            .onDelete('CASCADE')
        table.integer('partner_id')
            .unsigned()
            .references('id')
            .inTable('partners')
            .onDelete('CASCADE')
        table.integer('item_id')
            .unsigned()
            .references('id')
            .inTable('items')
            .onDelete('CASCADE')
    })
}

exports.down = function(knex, Promise) {
    return knex.schema.dropTable('orders')
}