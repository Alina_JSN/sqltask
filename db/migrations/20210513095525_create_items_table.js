exports.up = function(knex, Promise) {
    return knex.schema.createTable('items', function(table) {
        table.increments()
        table.string('item_name').notNullable()
        table.float('price').notNullable()
    })
}

exports.down = function(knex, Promise) {
    return knex.schema.dropTable('items')
}