exports.up = function(knex, Promise) {
    return knex.schema.createTable('users', function(table) {
        table.increments()
        table.string('login').unique().notNullable()
        table.string('password').notNullable()
        table.string('ip').notNullable()
        table.boolean('active').notNullable().defaultTo(false)
        table.timestamp('registration_date').defaultTo(knex.fn.now())
        table.timestamp('last_visit_date').defaultTo(knex.fn.now())
    })
}

exports.down = function(knex, Promise) {
    return knex.schema.dropTable('users')
}