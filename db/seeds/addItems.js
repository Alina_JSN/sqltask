const faker = require('faker')

const createFakeItems= () => ({
    item_name: faker.commerce.productName(),
    price: faker.commerce.price(),
})

exports.seed = function(knex) {
    return knex('items').del()
        .then(function () {
            const fakeItems = []
            const desiredFakeItems = 2
            for(let i =0; i < desiredFakeItems; i++){
                fakeItems.push(createFakeItems())
            }
            return knex('items').insert(fakeItems)
        })
}