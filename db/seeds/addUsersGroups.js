const faker = require('faker')

exports.seed = async function (knex) {
    let users = await knex('users').select('id')
    await knex('user_group').del()

    const usersGroups = [
        {
            user_id: users[0].id,
            group_id: 4
        }
    ]

    users.shift()

    for (const user of users) {
        const groupsCount = faker.random.arrayElement(['1', '2', '3'])
        const possibleIds = ['1', '2', '3']

        for (let i = 1; i <= groupsCount; i++) {
            const groupId = faker.random.arrayElement(possibleIds)
            usersGroups.push({
                user_id: user.id,
                group_id: groupId
            })

            possibleIds.splice(possibleIds.indexOf(groupId), 1)
        }
    }

    return await knex('user_group').insert(usersGroups)
}
