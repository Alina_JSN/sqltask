const faker = require('faker')

function generateIp(max) {
    return Math.floor(Math.random() * max)
}

let firstIndex = generateIp(255)

const getRandomIp = (firstIndex) => {
    return `${firstIndex}.${(generateIp(255))}.${(generateIp(255))}.${(generateIp(255))}`
}

const createFakeUser = (firstIndex) => ({
    login: faker.unique(faker.internet.userName),
    password: faker.internet.password(),
    ip: getRandomIp(firstIndex),
    active: faker.datatype.boolean(),
    registration_date: faker.date.between('2017-01-01', '2018-01-01'),
    last_visit_date: faker.date.between('2018-01-01', '2018-12-31'),
})

exports.seed = function (knex) {
    return knex('users').del()
        .then(function () {
            const fakeUsers = []
            const desiredFakeUsers = 20
            for (let i = 0; i < desiredFakeUsers; i++) {
                if (i % 9 === 0 && i !== 0) {
                    firstIndex = generateIp(255)
                    fakeUsers.push(createFakeUser(firstIndex))
                }
                fakeUsers.push(createFakeUser(firstIndex))
            }
            return knex.batchInsert('users', fakeUsers, 40)
        }).catch(err => console.log('Error seeds users', err))


}

