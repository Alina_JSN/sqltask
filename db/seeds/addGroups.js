const groups = [
    {id: 1, group_name: 'temporary'},
    {id: 2, group_name: 'regular'},
    {id: 3, group_name: 'editors'},
    {id: 4, group_name: 'admin'},
]

exports.seed = async function(knex) {
    await knex('group').del()
    return await knex('group').insert(groups)
}


