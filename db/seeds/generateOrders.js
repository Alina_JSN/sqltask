const faker = require('faker')

const ordersNumber = 50

exports.seed = async function(knex) {
    let users = await knex('users').select('id')

    let items = await knex('items').select('id')

    let partners = await knex('partners').select('id')

    let possiblePartners = partners.map(p => p.id)
    possiblePartners.push(null)

    return knex('orders').del()
        .then(function () {
            const orders = []
            for (let i = 0; i < ordersNumber; i++) {
                orders.push({
                    user_id: faker.random.arrayElement(users).id,
                    partner_id: faker.random.arrayElement(possiblePartners),
                    item_id: faker.random.arrayElement(items).id,
                })
            }
            return knex("orders").insert(orders)
        })
}
