const faker = require('faker')

const createFakePartner = () => ({
    partner_name: faker.commerce.productName(),
})

exports.seed = function(knex) {
    return knex('partners').del()
        .then(function () {
            const fakePartner = []
            const desiredFakeItems = 10
            for(let i =0; i < desiredFakeItems; i++){
                fakePartner.push(createFakePartner())
            }
            return knex('partners').insert(fakePartner)
        })
}
