// Update with your config settings.
module.exports = {

  development: {
    client: 'mysql',
    connection: {
      host: 'localhost',
      port: '3307',
      database: 'firstweek',
      user:     'root',
      password: 'root'
    },
    migrations: {
          tableName: 'knex_migrations',
          directory:  './db/migrations'
        },
      seeds: {
          directory:  './db/seeds'
      }


  },

}
