const express = require('express')
const app = express()
const knex = require("knex")({
    client: 'mysql',
    connection: {
        host: 'localhost',
        port: '3307',
        database: 'firstweek',
        user: 'root',
        password: 'root'
    },
})

app.get('/', function (req, res) {
    res.send('Hello World')
})

app.listen(3000)


async function getactiveUsers() {
    return await knex('users').select('id', 'login', 'active').where('active', true)
}
async function runFunc() {
    let query = await getactiveUsers();
    console.log(query)
}

runFunc()